﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace ExaminationSystem
{
    public partial class course_registration : Form
    {
        MySqlConnection cs = new MySqlConnection("Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2");
        String piclocation = null;
        public course_registration()
        {
            InitializeComponent();
        }

        private void course_registration_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("Select unitID,unitName from course_units", cs);
            cs.Open();
            MySqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox1.Items.Add(DR[0]);
                comboBox2.Items.Add(DR[0]);
                comboBox3.Items.Add(DR[0]);
                comboBox4.Items.Add(DR[0]);
                comboBox5.Items.Add(DR[0]);
                comboBox6.Items.Add(DR[0]);
                comboBox7.Items.Add(DR[0]);
                comboBox8.Items.Add(DR[0]);
                comboBox9.Items.Add(DR[0]);
                comboBox10.Items.Add(DR[0]);

            }
            cs.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
                   if((textBox1.Text!="")){
                MySqlCommand cmd9 = new MySqlCommand("insert into courses_registered(studentID,unitID1,unitID2,unitID3,unitID4,unitID5,unitID6,unitID7, unitID8,unitID9,unitID10) values('" + textBox1.Text + "','" + comboBox1.SelectedItem + "','" + comboBox2.SelectedItem + "','" + comboBox3.SelectedItem + "','" + comboBox4.SelectedItem + "','" + comboBox5.SelectedItem + "','" + comboBox6.SelectedItem + "','" + comboBox7.SelectedItem + "','" + comboBox8.SelectedItem + "','" + comboBox9.SelectedItem + "','" + comboBox10.SelectedItem + "')", cs);
                cs.Open();
                MySqlDataReader Df = cmd9.ExecuteReader();
                while (Df.Read())
                {
                }
                textBox1.Clear();
                comboBox1.SelectedItem=null;
                comboBox2.SelectedItem=null;
                comboBox3.SelectedItem=null;
                comboBox4.SelectedItem=null;
                comboBox5.SelectedItem=null;
                comboBox6.SelectedItem=null;
                comboBox7.SelectedItem=null;
                comboBox8.SelectedItem = null;
                comboBox9.SelectedItem = null;
                comboBox10.SelectedItem = null;
            }
            else{
                DialogResult dr = MessageBox.Show("Please Fill all the necessary details!!");
            }

            cs.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            admin_dashboard fm = new admin_dashboard();
            fm.Show();
        }
        
    }
}
