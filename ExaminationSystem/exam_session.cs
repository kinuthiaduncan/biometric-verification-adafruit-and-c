﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO.Ports;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace ExaminationSystem
{
    public partial class exam_session : Form
    {
        MySqlConnection cs = new MySqlConnection("Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2");
        public exam_session()
        {
            InitializeComponent();
        }

        private void exam_session_Load(object sender, EventArgs e)
        {
            int present = 0 ;
            int absent=0;
            String unit = null;
            unit = Unit(unit);
            present = presentStudents(present);
            absent = absentStudents(absent);
            textBox2.Text = present.ToString();
            textBox1.Text = unit.ToString();
            textBox5.Text = absent.ToString();
            MySqlCommand cmd88 = new MySqlCommand("SELECT  *  FROM timetable WHERE unitID=@unit ", cs);
            cmd88.Parameters.AddWithValue("@unit", unit);
            cs.Open();
            MySqlDataReader Dk = cmd88.ExecuteReader();
            while (Dk.Read())
            {
                textBox3.Text = ((Dk["examFrom"]).ToString());
                textBox4.Text = ((Dk["examTo"]).ToString());
            }
            cs.Close();
        }
        private String Unit(String unit)
        {
            MySqlCommand cmd7 = new MySqlCommand("SELECT * FROM progress WHERE progressID = '1'", cs);
            cs.Open();
            MySqlDataReader Dd = cmd7.ExecuteReader();
            while (Dd.Read())
            {
                unit = (Dd["unit"].ToString());
            }
            cs.Close();
            return unit;
        }
        private int presentStudents(int present)
        {
            String unit = null;
            int examID = 0;
            int studentspresent = 0;
            unit = Unit(unit);
            MySqlCommand cmd8 = new MySqlCommand("SELECT  examID  FROM timetable WHERE unitID=@unit ", cs);
            cmd8.Parameters.AddWithValue("@unit", unit);
            cs.Open();
            MySqlDataReader De = cmd8.ExecuteReader();
            while (De.Read())
            {
                examID = Convert.ToInt32(De["examID"]);
            }
            cs.Close();

            MySqlCommand cmd = new MySqlCommand("SELECT  COUNT(studentID) AS present  FROM exam_attendance WHERE examID=@examID ", cs);
            cmd.Parameters.AddWithValue("@examID", examID);
            cs.Open();
            MySqlDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                studentspresent = Convert.ToInt32(DR["present"]);
            }
            cs.Close();
            return studentspresent;
        }
        private int absentStudents(int absent)
        {
            int studentsabsent = 0;
            int present = 0;
            string unit = null;
            int studentsregistered = 0;
            present = presentStudents(present);
            unit = Unit(unit);
            MySqlCommand cmd = new MySqlCommand("SELECT  COUNT(studentID) AS registered  FROM courses_registered WHERE (unitID1=@examID) OR (unitID2=@examID) OR (unitID3=@examID) OR (unitID4=@examID) OR (unitID5=@examID) OR (unitID6=@examID)OR (unitID7=@examID)OR(unitID8=@examID)OR(unitID9=@examID)OR(unitID10=@examID)", cs);
            cmd.Parameters.AddWithValue("@examID", unit);
            cs.Open();
            MySqlDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                studentsregistered = Convert.ToInt32(DR["registered"]);
            }
            cs.Close();
            studentsabsent = studentsregistered - present;
            return studentsabsent;
        }
        private void printAttendance_Click(object sender, EventArgs e)
        {
            int present = 0;
            int absent = 0;
            String unit = null;
            String examdate = null;
            String examto = null;
            String examfrom = null;
            String supervisor = null;
            unit = Unit(unit);
            present = presentStudents(present);
            absent = absentStudents(absent);

            MySqlCommand cmd88 = new MySqlCommand("SELECT  *  FROM timetable WHERE unitID=@unit ", cs);
            cmd88.Parameters.AddWithValue("@unit", unit);
            cs.Open();
            MySqlDataReader Dk = cmd88.ExecuteReader();
            while (Dk.Read())
            {
                examdate = ((DateTime)Dk["examDate"]).ToString("d");
              examto = (Dk["examto"].ToString());
              examfrom = (Dk["examFrom"].ToString());
               supervisor = (Dk["supervisor"].ToString());
            }
            cs.Close();

            string outputFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "attendance.pdf");
            using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (Document doc = new Document(PageSize.A4))
                {
                    using (PdfWriter w = PdfWriter.GetInstance(doc, fs))
                    {
                        doc.Open();
                        iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance("C:\\uploads\\logo.gif");
                        logo.ScaleAbsolute(250, 300);
                        logo.Alignment = Element.ALIGN_CENTER;
                        doc.Add(logo);
                        Paragraph para = new Paragraph("UNIVERSITY OF NAIROBI");
                        para.Font.Size = 20;
                        para.Alignment = Element.ALIGN_CENTER;
                        doc.Add(para);
                        Paragraph para3 = new Paragraph(" ");
                        para3.Font.Size = 20;
                        doc.Add(para3);
                        Paragraph para2 = new Paragraph("Students Examinations Attendance List");
                        para2.Alignment = Element.ALIGN_CENTER;
                        doc.Add(para2);
                        Paragraph para4 = new Paragraph(" ");
                        para4.Font.Size = 30;
                        doc.Add(para4);
                        Paragraph para5 = new Paragraph("Course Unit: "+unit+" ");
                        para5.IndentationLeft = 25;
                        para5.Alignment = Element.ALIGN_LEFT;
                        doc.Add(para5);
                        Paragraph para6 = new Paragraph(" ");
                        para6.Font.Size = 18;
                        doc.Add(para6);

                        Paragraph para55 = new Paragraph("Supervisor: " + supervisor + " ");
                        para55.IndentationLeft = 25;
                        para55.Alignment = Element.ALIGN_LEFT;
                        doc.Add(para55);
                        Paragraph para65 = new Paragraph(" ");
                        para65.Font.Size = 18;
                        doc.Add(para65);

                        Paragraph para7 = new Paragraph("Date Taken: "+examdate+" ");
                        para7.IndentationLeft = 25;
                        para7.Alignment = Element.ALIGN_LEFT; ;
                        doc.Add(para7);
                        Paragraph para8 = new Paragraph(" ");
                        para8.Font.Size = 18;
                        doc.Add(para8);
                        Paragraph para9 = new Paragraph("Time: "+examfrom+" to "+examto+" ");
                        para9.IndentationLeft = 25;
                        para9.Alignment = Element.ALIGN_LEFT; ;
                        doc.Add(para9);
                        Paragraph para10 = new Paragraph(" ");
                        para10.Font.Size = 18;
                        doc.Add(para10);
                       Paragraph para11 = new Paragraph("Students Present: "+present+" ");
                        para11.IndentationLeft = 25;
                        para11.Alignment = Element.ALIGN_LEFT; ;
                        doc.Add(para11);
                        Paragraph para12 = new Paragraph(" ");
                        para12.Font.Size = 18;
                        doc.Add(para12);
                        Paragraph para13 = new Paragraph("Students Absent: "+absent+" ");
                        para13.Alignment = Element.ALIGN_LEFT;
                        para13.IndentationLeft = 25;
                        doc.Add(para13);
                        doc.NewPage();
                        Paragraph para20 = new Paragraph("Present and Fully Credible Students");
                        para20.Alignment = Element.ALIGN_CENTER;
                        doc.Add(para20);
                        doc.Add(para12);
                        PdfPTable t = new PdfPTable(2);                                          
                        t.AddCell("Student Reg. Number");
                        t.AddCell("Time Verified");
                        MySqlCommand cmd2 = new MySqlCommand("SELECT * FROM exam_attendance WHERE unit = @unit and status='1' ", cs);
                        cmd2.Parameters.AddWithValue("@unit", unit);
                        cs.Open();
                        MySqlDataReader Da = cmd2.ExecuteReader();
                        while (Da.Read())
                        {
                          String  student = (Da["studentID"].ToString());
                          String timeVerified = (Da["entryTime"].ToString());
                          String status = (Da["status"].ToString());
                          t.AddCell(student);
                          t.AddCell(timeVerified);
                        }
                        cs.Close();                   
                        doc.Add(t);
                        doc.Add(para12);
                        Paragraph para21 = new Paragraph("Present and Non-Credible Students");
                        para21.Alignment = Element.ALIGN_CENTER;
                        doc.Add(para21);
                        doc.Add(para12);
                        PdfPTable u = new PdfPTable(2);
                        u.AddCell("Student Reg. Number");
                        u.AddCell("Time Verified");
                        MySqlCommand cmd22 = new MySqlCommand("SELECT * FROM exam_attendance WHERE unit = @unit and status='0' ", cs);
                        cmd22.Parameters.AddWithValue("@unit", unit);
                        cs.Open();
                        MySqlDataReader Dw = cmd22.ExecuteReader();
                        while (Dw.Read())
                        {
                            String student = (Dw["studentID"].ToString());
                            String timeVerified = (Dw["entryTime"].ToString());
                            String status = (Dw["status"].ToString());
                            u.AddCell(student);
                            u.AddCell(timeVerified);
                        }
                        cs.Close();
                        doc.Add(u);

/*...........................................................................................*/
                        doc.Add(para12);
                        Paragraph para22 = new Paragraph("Absent Students");
                        para22.Alignment = Element.ALIGN_CENTER;
                        doc.Add(para22);
                        doc.Add(para12);


                        PdfPTable v = new PdfPTable(2);
                        v.AddCell("Student Reg. Number");
                        v.AddCell("Time Verified");
                        
                                                MySqlCommand cmd23 = new MySqlCommand("SELECT * FROM courses_registered WHERE studentID NOT IN (SELECT studentID FROM exam_attendance WHERE unit=@unit) ", cs);
                                                cmd23.Parameters.AddWithValue("@unit", unit);
                                                cs.Open();
                                                MySqlDataReader Dx = cmd23.ExecuteReader();
                                                while (Dx.Read())
                                                {
                                                    String mystudent = (Dx["studentID"].ToString());
                                                    v.AddCell(mystudent);
                                                    v.AddCell("N/A");
                                                }
                                                cs.Close();
                                                
                        doc.Add(v);

                        doc.Close();
                    }
                }
            }
            MessageBox.Show("PDF file exported to Desktop as attendance.pdf.","Success!!");
        }
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            dashboard fm = new dashboard();
            fm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure that you want to log out?", "Please Confirm Before Exit", MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    Application.Exit();
                    break;
                case DialogResult.No:
                    this.Hide();
                    exam_session fn = new exam_session();
                    fn.Show();
                    break;
            }
        }
    }
}
