﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO.Ports;

namespace ExaminationSystem
{
    public partial class courses : Form
    {
        string myconnection = "Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2";
        public courses()
        {
            InitializeComponent();
        }
       
        private void CoursesSave_Click(object sender, EventArgs e)
        {
            if (courseID.Text == "")
            {
                MessageBox.Show("Please Fill the Course ID!");
                return;
            }
            else if (courseName.Text == "")
            {
                MessageBox.Show("Please Fill the Course Name!");
                return;
            }
            else if (cost.Text == "")
            {
                MessageBox.Show("Please Fill the Course Cost!");
                return;
            }
            try
            {
                string query = "insert into courses(courseID, courseName, government, self) values('" + this.courseID.Text + "','" + this.courseName.Text + "','" + Convert.ToDecimal(this.cost.Text) + "','" + Convert.ToDecimal(this.textBox1.Text) + "' )";
                MySqlConnection cs = new MySqlConnection(myconnection);
                MySqlCommand mycommand = new MySqlCommand(query, cs);
                MySqlDataReader myreader;
                cs.Open();
                myreader = mycommand.ExecuteReader();
                MessageBox.Show("All Entered Courses have been saved");
                cs.Close();
                courseID.Clear();
                courseName.Clear();
                cost.Clear();
                textBox1.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void CoursesExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            admin_dashboard fm = new admin_dashboard();
            fm.Show();
        }
    }
}
