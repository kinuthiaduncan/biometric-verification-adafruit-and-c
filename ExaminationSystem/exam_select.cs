﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace ExaminationSystem
{
    public partial class exam_select : Form
    {
        MySqlConnection cs = new MySqlConnection("Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2");
        public exam_select()
        {
            InitializeComponent();
        }
        private void exitSelect_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void continueSelect_Click(object sender, EventArgs e)
        {
            string Query = "update progress set unit='" + comboBox1.SelectedItem.ToString() + "' where progressID='1';";
            cs.Open();
            MySqlCommand MyCommand2 = new MySqlCommand(Query, cs);
            MySqlDataReader MyReader2;
            MyReader2 = MyCommand2.ExecuteReader(); 
            this.Hide();
            cs.Close();
            dashboard fm = new dashboard();
            fm.Show();
        }
        private void populate_Click(object sender, EventArgs e)
        {
           // string course = comboBox2.SelectedValue.ToString();
            MySqlCommand cmd = new MySqlCommand("Select unitID from course_units WHERE courseID ='" + comboBox2.SelectedItem.ToString() + "'", cs);
            cs.Open();  
            MySqlDataReader DR = cmd.ExecuteReader();
                comboBox1.Items.Clear();
                while (DR.Read())
                {
                    comboBox1.Items.Add(DR[0]);

                }
            
            cs.Close();
        }
        private void populateCourse_Click(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("Select courseID,courseName from courses", cs);
            cs.Open();
            MySqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox2.Items.Add(DR[0]);

            }
            cs.Close();
        }

        
    }
}
