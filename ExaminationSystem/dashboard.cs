﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO.Ports;

namespace ExaminationSystem
{
    public partial class dashboard : Form
    {
        MySqlConnection cs = new MySqlConnection("Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2");
        SerialPort port;
        public dashboard()
        {
            InitializeComponent();
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dashboard_Load(object sender, EventArgs e)
        {
            port = new SerialPort("COM5", 9600);
            port.Open();
            int coursestatus = 0;
            int feebalance = 0; ;
            int feestatus=0;
            int studentstatus = 0;
            try
            {
               
               port.Write("p");
                String myVar = port.ReadLine();
                Char myVar2 = myVar[0];
                MySqlCommand cmd = new MySqlCommand("SELECT * from students where fingerprintID=@myVar", cs);
                cmd.Parameters.AddWithValue("@myVar", myVar);
                MySqlDataAdapter da = new MySqlDataAdapter();
                cs.Open();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                da.Fill(ds);
                dt = ds.Tables[0];
                cs.Close();
                foreach (DataRow dr in dt.Rows)
                {
                    textBox2.Text=(dr["studentID"].ToString());
                    textBox3.Text = (dr["surname"].ToString());
                    textBox4.Text = (dr["otherNames"].ToString());
                    textBox5.Text = (dr["idNo"].ToString());
                    textBox6.Text = (dr["gender"].ToString());
                    textBox7.Text = (dr["phone"].ToString());
                    textBox8.Text = (dr["courseID"].ToString());
                    textBox9.Text = (dr["academicLevel"].ToString());
                    textBox10.Text = (dr["year"].ToString());
                    coursestatus = coursesVerification(coursestatus);
                    feebalance=feeBalance(feebalance);
                    feestatus=feesVerification(feestatus);
                    string path =(string)@dr["passportPhoto"];
                   pictureBox1.Image = Image.FromFile(path);
                   if ((coursestatus == 2) && (feestatus==2))
                   {
                        string path2 = (@"C:\uploads\signs\tick.png");
                        pictureBox2.Image = Image.FromFile(path2);
                        studentstatus = 1;
                        textBox12.Text = studentstatus.ToString();
                   }
                   else if ((coursestatus == 3)||(feestatus==3))
                   {
                        string path2 = (@"C:\uploads\signs\cross.png");
                        pictureBox2.Image = Image.FromFile(path2);
                        studentstatus = 0;
                        textBox12.Text = studentstatus.ToString();
                   }
                   switch (coursestatus)
                   {
                       case 2:
                           textBox1.Text = "You have registered for the Unit";
                           break;
                       case 3:
                           textBox1.Text = "You have not registered for the unit!";
                           break;
                       default:
                           textBox1.Text = "An error occurred!";
                           break;
                   }
                   if (feebalance > 0)
                   {
                       textBox11.Text = "Your Fee Balance is: " + feebalance.ToString() + "  ";
                   }
                   else
                   {
                       textBox11.Text = "You have cleared your fees";
                   }
                }
                
                port.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         }

        private void next_Click(object sender, EventArgs e)
        {
          //  port = new SerialPort("COM5", 9600);
            port.Open();
            int coursestatus = 0;
            int feebalance = 0; ;
            int feestatus = 0;
            int studentstatus = 0;
            String unit = null;
            String supervisor=null;
            int examID = 0;
            try
            {
                MySqlCommand cmd7 = new MySqlCommand("SELECT * FROM progress WHERE progressID = '1'", cs);
                cs.Open();
                MySqlDataReader Dd = cmd7.ExecuteReader();
                while (Dd.Read())
                {
                    unit = (Dd["unit"].ToString());
                    supervisor = (Dd["supervisor"].ToString());
                }
                cs.Close();

                MySqlCommand cmd8 = new MySqlCommand("SELECT  examID  FROM timetable WHERE unitID=@unit ", cs);
                cmd8.Parameters.AddWithValue("@unit", unit);
                cs.Open();
                MySqlDataReader De = cmd8.ExecuteReader();
                while (De.Read())
                {
                    examID = Convert.ToInt32(De["examID"]);
                }
                cs.Close();

                MySqlCommand cmd9 = new MySqlCommand("insert into exam_attendance(examID,studentID,unit,status,entryTime,supervisor) values('" + examID + "','" + textBox2.Text + "','" + unit + "','" + textBox12.Text + "','" + DateTime.Now.ToString("HH:mm:ss tt") + "','" + supervisor + "')", cs);
                cs.Open();
                MySqlDataReader Df = cmd9.ExecuteReader();
                while (Df.Read())
                {   
                }
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                textBox6.Clear();
                textBox7.Clear();
                textBox8.Clear();
                textBox9.Clear();
                textBox10.Clear();
                textBox11.Clear();
                textBox12.Clear();
                pictureBox1.Image = null;
                pictureBox2.Image = null;
                cs.Close(); 
                port.Write("p");
                String myVar = port.ReadLine();
                Char myVar2 = myVar[0];
                MySqlCommand cmd = new MySqlCommand("SELECT * from students where fingerprintID=@myVar", cs);
                cmd.Parameters.AddWithValue("@myVar", myVar);
                MySqlDataAdapter da = new MySqlDataAdapter();
                cs.Open();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                da.Fill(ds);
                dt = ds.Tables[0];
                cs.Close();
                foreach (DataRow dr in dt.Rows)
                {
                    textBox2.Text = (dr["studentID"].ToString());
                    textBox3.Text = (dr["surname"].ToString());
                    textBox4.Text = (dr["otherNames"].ToString());
                    textBox5.Text = (dr["idNo"].ToString());
                    textBox6.Text = (dr["gender"].ToString());
                    textBox7.Text = (dr["phone"].ToString());
                    textBox8.Text = (dr["courseID"].ToString());
                    textBox9.Text = (dr["academicLevel"].ToString());
                    textBox10.Text = (dr["year"].ToString());
                    coursestatus = coursesVerification(coursestatus);
                    feebalance = feeBalance(feebalance);
                    feestatus = feesVerification(feestatus);
                    string path = (string)@dr["passportPhoto"];
                    pictureBox1.Image = Image.FromFile(path);
                    if ((coursestatus == 2) && (feestatus == 2))
                    {
                        string path2 = (@"C:\uploads\signs\tick.png");
                        pictureBox2.Image = Image.FromFile(path2);
                        studentstatus = 1;
                        textBox12.Text = studentstatus.ToString();
                    }
                    else if ((coursestatus == 3) || (feestatus == 3))
                    {
                        string path2 = (@"C:\uploads\signs\cross.png");
                        pictureBox2.Image = Image.FromFile(path2);
                        studentstatus = 0;
                        textBox12.Text = studentstatus.ToString();
                    }
                    switch (coursestatus)
                    {
                        case 2:
                            textBox1.Text = "You have registered for the Unit";
                            break;
                        case 3:
                            textBox1.Text = "You have not registered for the unit!";
                            break;
                        default:
                            textBox1.Text = "An error occurred!";
                            break;
                    }
                    if (feebalance > 0)
                    {
                        textBox11.Text = "Your Fee Balance is: '" + feebalance.ToString() + " ' ";
                    }
                    else
                    {
                        textBox11.Text = "You have cleared your fees";
                    }
                }

                port.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private int coursesVerification(int status)
        {             
            String s = null;
            String a = null;
            String b = null;
            String c = null;
            String d = null;
            String e = null;
            String f = null;
            String g = null;
            String h = null;
            String i = null;
            String j = null;
            
            MySqlCommand cmd2 = new MySqlCommand("SELECT unit FROM progress WHERE progressID = '1'",cs);
            cs.Open();
                MySqlDataReader Da = cmd2.ExecuteReader();
                while (Da.Read())
                {
                     s = (Da["unit"].ToString());
                }
                cs.Close();
                MySqlCommand cmd = new MySqlCommand("Select * from courses_registered where studentID=@username ", cs);
                cmd.Parameters.AddWithValue("@username", textBox2.Text);
            cs.Open();
                MySqlDataReader DR = cmd.ExecuteReader();
                while (DR.Read())
                {
                    a = (DR["unitID1"].ToString());
                    b = (DR["unitID2"].ToString());
                    c = (DR["unitID3"].ToString());
                    d = (DR["unitID4"].ToString());
                    e = (DR["unitID5"].ToString());
                    f = (DR["unitID6"].ToString());
                    g = (DR["unitID7"].ToString());
                    h = (DR["unitID8"].ToString());
                    i = (DR["unitID9"].ToString());
                    j = (DR["unitID10"].ToString());
                }
                if ((s.Equals(a)) || (s.Equals(b)) || (s.Equals(c)) || (s.Equals(d)) || (s.Equals(e)) || (s.Equals(f)) || (s.Equals(g)) || (s.Equals(h)) || (s.Equals(i)) || (s.Equals(j)))
                    {
                        status = 2;
                    }
                    else
                    {
                        status = 3;
                    }
                cs.Close();
                 return status;
               }
        private int feeBalance(int balance)
        {
            int feepaid=0;
            int feerequired = 0;
            String a="government";
            String b="Self-Sponsored";
            String sponsorship = null; ;
            MySqlCommand cmd2 = new MySqlCommand("SELECT SUM(amountPaid) As amount FROM fees WHERE studentID=@username ", cs);
                cmd2.Parameters.AddWithValue("@username", textBox2.Text);
                 cs.Open();
                MySqlDataReader DR = cmd2.ExecuteReader();
                while (DR.Read())
                
            {
                feepaid = Convert.ToInt32((DR["amount"]));
            }
            cs.Close();
            MySqlCommand cmd3 = new MySqlCommand("SELECT  sponsorship  FROM students WHERE studentID=@student ", cs);
            cmd3.Parameters.AddWithValue("@student", textBox2.Text);
            cs.Open();
            MySqlDataReader DB = cmd3.ExecuteReader();
            while (DB.Read())
            {
                sponsorship = (DB["sponsorship"].ToString());
            }
            cs.Close();

            if((sponsorship.Equals(a))){
            MySqlCommand cmd = new MySqlCommand("SELECT  government  FROM courses WHERE courseID=@course ", cs);
            cmd.Parameters.AddWithValue("@course", textBox8.Text);
            cs.Open();
            MySqlDataReader DA = cmd.ExecuteReader();
            while (DA.Read())
            {
                feerequired = Convert.ToInt32((DA["government"]));
            }
            cs.Close();
            }
            else if ((sponsorship.Equals(b)))
            {
                MySqlCommand cmd = new MySqlCommand("SELECT  *  FROM courses WHERE courseID=@course ", cs);
                cmd.Parameters.AddWithValue("@course", textBox8.Text);
                cs.Open();
                MySqlDataReader DA = cmd.ExecuteReader();
                while (DA.Read())
                {
                    feerequired = Convert.ToInt32((DA["self"]));
                }
                cs.Close();
            }
            else
            {
                feerequired = 234;
            }
            balance = feerequired - feepaid;
            return balance;
        }
        private int feesVerification(int feestatus)
        {
            int feebalance = 0;
            feebalance = feeBalance(feebalance);
            if (feebalance > 0)
            {
                feestatus = 3;
            }
            else if(feebalance <= 0)
            {
                feestatus = 2;
            }
            return feestatus;
        }

        private void finishButton_Click(object sender, EventArgs e)
        {
            String unit = null;
            String supervisor = null;
            int examID = 0;

            MySqlCommand cmd7 = new MySqlCommand("SELECT * FROM progress WHERE progressID = '1'", cs);
            cs.Open();
            MySqlDataReader Dd = cmd7.ExecuteReader();
            while (Dd.Read())
            {
                unit = (Dd["unit"].ToString());
                supervisor = (Dd["supervisor"].ToString());
            }
            cs.Close();

            MySqlCommand cmd8 = new MySqlCommand("SELECT  examID  FROM timetable WHERE unitID=@unit ", cs);
            cmd8.Parameters.AddWithValue("@unit", unit);
            cs.Open();
            MySqlDataReader De = cmd8.ExecuteReader();
            while (De.Read())
            {
                examID = Convert.ToInt32(De["examID"]);
            }
            cs.Close();

            MySqlCommand cmd9 = new MySqlCommand("insert into exam_attendance(examID,studentID,unit,status,entryTime,supervisor) values('" + examID + "','" + textBox2.Text + "','" + unit + "','" + textBox12.Text + "','" + DateTime.Now.ToString("HH:mm:ss tt") + "','" + supervisor + "')", cs);
            cs.Open();
            MySqlDataReader Df = cmd9.ExecuteReader();
            while (Df.Read())
            {
            }
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
            textBox11.Clear();
            textBox12.Clear();
            pictureBox1.Image = null;
            pictureBox2.Image = null;
            cs.Close(); 
            DialogResult dr=MessageBox.Show("Are you sure that all students have been verified?","Please Confirm Before Exit",MessageBoxButtons.YesNo);
            switch (dr)
            {
                case DialogResult.Yes:
                    this.Hide();
                    exam_session fm = new exam_session();
                    fm.Show();
                    break;
                case DialogResult.No:
                    this.Hide();
                    dashboard fn = new dashboard();
                    fn.Show();
                    break;
            }

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void printAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pDFToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
