﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO.Ports;

namespace ExaminationSystem
{
    public partial class course_units : Form
    {
        string myconnection = "Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2";
        public course_units()
        {
            InitializeComponent();
        }
        private void nextButtonUnits_Click(object sender, EventArgs e)
        {
            if (unitIDUnits.Text == "")
            {
                MessageBox.Show("Please Fill the Unit ID!");
                return;
            }
            else if(unitNameUnits.Text == "")
            {
               MessageBox.Show("Please Fill the Unit Name!");
                return;
            }
            else if (courseUnits.Text == "")
            {
                MessageBox.Show("Please Fill the Course ID!");
                return;
            }
            else if (yearUnits.Text == "")
            {
                MessageBox.Show("Please fill the Unit Year!");
                return;
            }
            else if (academicLevelUnits.Text == "")
            {
                MessageBox.Show("Please fill the Unit Cost!");
                return;
            }
            try
            {
                string query = "insert into course_units(unitID, unitName,courseID,year,cost,academicLevel) values('" + this.unitIDUnits + "','" + this.unitNameUnits.Text + "','" + this.courseUnits.Text + "','" + this.yearUnits.Text + "','" + this.academicLevelUnits.Text + "', )";
                MySqlConnection cs = new MySqlConnection(myconnection);
                MySqlCommand mycommand = new MySqlCommand(query, cs);
                MySqlDataReader myreader;
                cs.Open();
                myreader = mycommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void saveButtonUnits_Click(object sender, EventArgs e)
        {
            if (unitIDUnits.Text == "")
            {
                MessageBox.Show("Please Fill the Unit ID!");
                return;
            }
            else if (unitNameUnits.Text == "")
            {
                MessageBox.Show("Please Fill the Unit Name!");
                return;
            }
            else if (courseUnits.Text == "")
            {
                MessageBox.Show("Please Fill the Course ID!");
                return;
            }
            else if (yearUnits.Text == "")
            {
                MessageBox.Show("Please fill the Unit Year!");
                return;
            }
            else if (academicLevelUnits.Text == "")
            {
                MessageBox.Show("Please fill the Unit Cost!");
                return;
            }
            try
            {
                string query = "insert into course_units(unitID, unitName,courseID,year,academicLevel) values('" + this.unitIDUnits + "','" + this.unitNameUnits.Text + "','" + this.courseUnits.SelectedItem + "','" + Convert.ToInt32(this.yearUnits.Text) + "','" + this.academicLevelUnits.SelectedItem + "')";
                MySqlConnection cs = new MySqlConnection(myconnection);
                MySqlCommand mycommand = new MySqlCommand(query, cs);
                MySqlDataReader myreader;
                cs.Open();
                myreader = mycommand.ExecuteReader();
                MessageBox.Show("All Entered Courses have been saved");
                unitIDUnits.Clear();
                unitNameUnits.Clear();
                yearUnits.Clear();
                courseUnits.SelectedItem = null;
                academicLevelUnits.SelectedItem = null;

                cs.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void exitButtonUnits_Click(object sender, EventArgs e)
        {
            this.Hide();
            admin_dashboard fm = new admin_dashboard();
            fm.Show();
        }
        private void courseUnitsForm_Load(object sender, EventArgs e)
        {
            string query = "select courseID from courses";
            MySqlConnection cs = new MySqlConnection(myconnection);
            MySqlCommand mycommand = new MySqlCommand(query, cs);
            cs.Open();
            MySqlDataReader DR = mycommand.ExecuteReader();

            while (DR.Read())
            {
                courseUnits.Items.Add(DR[0]);

            }
            cs.Close();
        }
    }
}
