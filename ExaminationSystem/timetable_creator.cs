﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO.Ports;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace ExaminationSystem
{
    public partial class timetable_creator : Form
    {
        string myconnection = "Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2";
        MySqlConnection cs = new MySqlConnection("Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2");
        public timetable_creator()
        {
            InitializeComponent();
        }

        private void timetable_creator_Load(object sender, EventArgs e)
        {
            string query = "select unitID from course_units";
            MySqlConnection cs = new MySqlConnection(myconnection);
            MySqlCommand mycommand = new MySqlCommand(query, cs);
            cs.Open();
            MySqlDataReader DR = mycommand.ExecuteReader();

            while (DR.Read())
            {
                comboBox1.Items.Add(DR[0]);

            }
            cs.Close();

            MySqlCommand cmd = new MySqlCommand("SELECT  lecturerID  FROM lecturers", cs);
            cs.Open();
            MySqlDataReader De = cmd.ExecuteReader();
            while (De.Read())
            {
                comboBox2.Items.Add(De[0]);
            }
            cs.Close();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (dateTimePicker1.Text == "")
            {
                MessageBox.Show("Please Fill a Date for the Exam!");
                return;
            }
            else if (comboBox1.SelectedItem == "")
            {
                MessageBox.Show("Please Fill the Unit!");
                return;
            }
            else if (dateTimePicker2.Text == "")
            {
                MessageBox.Show("Please Fill the Exam Time!");
                return;
            }
            else if (dateTimePicker3.Text == "")
            {
                MessageBox.Show("Please fill the Exam Time!");
                return;
            }
            else if (textBox3.Text == "")
            {
                MessageBox.Show("Please fill the Exam Venue!");
                return;
            }
            else if (comboBox2.SelectedItem == "")
            {
                MessageBox.Show("Please fill the Exam Supervisor!");
                return;
            }
            try
            {

                string query = "insert into timetable(unitID, examDate,examFrom,examTo,venue,supervisor) values('" + comboBox1.SelectedItem + "','" + dateTimePicker1.Text + "','" + dateTimePicker2.Text + "','" + dateTimePicker3.Text + "','" + textBox3.Text + "','" + comboBox2.SelectedItem + "' )";
                MySqlConnection cs = new MySqlConnection(myconnection);
                MySqlCommand mycommand = new MySqlCommand(query, cs);
                MySqlDataReader myreader;
                cs.Open();
                myreader = mycommand.ExecuteReader();
                cs.Close();

                comboBox1.SelectedItem = null;
                comboBox2.SelectedItem = null;
                textBox3.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            admin_dashboard fm = new admin_dashboard();
            fm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string outputFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "timetable.pdf");
            using (FileStream fs = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (Document doc = new Document(PageSize.A3))
                {
                    using (PdfWriter w = PdfWriter.GetInstance(doc, fs))
                    {
                        doc.Open();
                        Paragraph para = new Paragraph("UNIVERSITY OF NAIROBI");
                        para.Font.Size = 20;
                        para.Alignment = Element.ALIGN_CENTER;
                        doc.Add(para);
                        Paragraph para3 = new Paragraph(" ");
                        para3.Font.Size = 10;
                        doc.Add(para3);
                        Paragraph para2 = new Paragraph("EXAMINATION TIMETABLE");
                        para2.Font.Size = 15;
                        para2.Alignment = Element.ALIGN_CENTER;
                        doc.Add(para2);
                        doc.Add(para3);
                        PdfPTable t = new PdfPTable(5);
                        t.AddCell("UNIT");
                        t.AddCell("DATE");
                        t.AddCell("FROM");
                        t.AddCell("TO");
                        t.AddCell("VENUE");

                        MySqlCommand cmd22 = new MySqlCommand("SELECT * FROM timetable ", cs);
                        cs.Open();
                        MySqlDataReader Dw = cmd22.ExecuteReader();
                        while (Dw.Read())
                        {
                            String unit = (Dw["unitID"].ToString());
                            String date = ((DateTime)Dw["examDate"]).ToString("d");
                            String from = (Dw["examFrom"].ToString());
                            String to = (Dw["examTo"].ToString());
                            String venue = (Dw["venue"].ToString());
                            t.AddCell(unit);
                            t.AddCell(date);
                            t.AddCell(from);
                            t.AddCell(to);
                            t.AddCell(venue);
                        }
                        cs.Close();
                        doc.Add(t);
                        doc.Close();
                    }
                }
            }

            MessageBox.Show("Timetable exported to Desktop as timetable.pdf");
        }

    }
}
