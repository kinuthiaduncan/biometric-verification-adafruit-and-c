﻿namespace ExaminationSystem
{
    partial class course_units
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.unitIDUnits = new System.Windows.Forms.TextBox();
            this.yearUnits = new System.Windows.Forms.TextBox();
            this.unitNameUnits = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.academicLevelUnits = new System.Windows.Forms.ComboBox();
            this.courseUnits = new System.Windows.Forms.ComboBox();
            this.exitButtonUnits = new System.Windows.Forms.Button();
            this.saveButtonUnits = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // unitIDUnits
            // 
            this.unitIDUnits.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unitIDUnits.Location = new System.Drawing.Point(262, 57);
            this.unitIDUnits.Name = "unitIDUnits";
            this.unitIDUnits.Size = new System.Drawing.Size(276, 27);
            this.unitIDUnits.TabIndex = 0;
            // 
            // yearUnits
            // 
            this.yearUnits.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yearUnits.Location = new System.Drawing.Point(262, 196);
            this.yearUnits.Name = "yearUnits";
            this.yearUnits.Size = new System.Drawing.Size(276, 27);
            this.yearUnits.TabIndex = 1;
            // 
            // unitNameUnits
            // 
            this.unitNameUnits.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unitNameUnits.Location = new System.Drawing.Point(262, 104);
            this.unitNameUnits.Name = "unitNameUnits";
            this.unitNameUnits.Size = new System.Drawing.Size(276, 27);
            this.unitNameUnits.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkBlue;
            this.label1.Location = new System.Drawing.Point(78, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "Unit ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkBlue;
            this.label2.Location = new System.Drawing.Point(78, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "Unit Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkBlue;
            this.label3.Location = new System.Drawing.Point(78, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "Course";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DarkBlue;
            this.label4.Location = new System.Drawing.Point(78, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "Year";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DarkBlue;
            this.label5.Location = new System.Drawing.Point(78, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "Academic Level";
            // 
            // academicLevelUnits
            // 
            this.academicLevelUnits.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.academicLevelUnits.FormattingEnabled = true;
            this.academicLevelUnits.Items.AddRange(new object[] {
            "Undergraduate",
            "Postgraduate"});
            this.academicLevelUnits.Location = new System.Drawing.Point(262, 241);
            this.academicLevelUnits.Name = "academicLevelUnits";
            this.academicLevelUnits.Size = new System.Drawing.Size(276, 27);
            this.academicLevelUnits.TabIndex = 11;
            // 
            // courseUnits
            // 
            this.courseUnits.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.courseUnits.FormattingEnabled = true;
            this.courseUnits.Location = new System.Drawing.Point(262, 150);
            this.courseUnits.Name = "courseUnits";
            this.courseUnits.Size = new System.Drawing.Size(276, 27);
            this.courseUnits.TabIndex = 12;
            // 
            // exitButtonUnits
            // 
            this.exitButtonUnits.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButtonUnits.ForeColor = System.Drawing.Color.Red;
            this.exitButtonUnits.Location = new System.Drawing.Point(427, 312);
            this.exitButtonUnits.Name = "exitButtonUnits";
            this.exitButtonUnits.Size = new System.Drawing.Size(91, 79);
            this.exitButtonUnits.TabIndex = 14;
            this.exitButtonUnits.Text = "EXIT";
            this.exitButtonUnits.UseVisualStyleBackColor = true;
            // 
            // saveButtonUnits
            // 
            this.saveButtonUnits.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButtonUnits.ForeColor = System.Drawing.Color.DarkBlue;
            this.saveButtonUnits.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.saveButtonUnits.Location = new System.Drawing.Point(228, 331);
            this.saveButtonUnits.Name = "saveButtonUnits";
            this.saveButtonUnits.Size = new System.Drawing.Size(90, 40);
            this.saveButtonUnits.TabIndex = 15;
            this.saveButtonUnits.Text = "SAVE";
            this.saveButtonUnits.UseVisualStyleBackColor = true;
            this.saveButtonUnits.Click += new System.EventHandler(this.saveButtonUnits_Click);
            // 
            // course_units
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(597, 407);
            this.Controls.Add(this.saveButtonUnits);
            this.Controls.Add(this.exitButtonUnits);
            this.Controls.Add(this.courseUnits);
            this.Controls.Add(this.academicLevelUnits);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.unitNameUnits);
            this.Controls.Add(this.yearUnits);
            this.Controls.Add(this.unitIDUnits);
            this.Name = "course_units";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Course Units";
            this.Load += new System.EventHandler(this.courseUnitsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox unitIDUnits;
        private System.Windows.Forms.TextBox yearUnits;
        private System.Windows.Forms.TextBox unitNameUnits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox academicLevelUnits;
        private System.Windows.Forms.ComboBox courseUnits;
        private System.Windows.Forms.Button exitButtonUnits;
        private System.Windows.Forms.Button saveButtonUnits;
    }
}