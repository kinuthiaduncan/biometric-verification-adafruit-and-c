﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace ExaminationSystem
{
    public partial class students_enroll : Form
    {
        MySqlConnection cs = new MySqlConnection("Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2");
        String piclocation = null;
        public students_enroll()
        {
            InitializeComponent();
        }
        private void picturebox_Click(object sender, EventArgs e)
        {

            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp;*.png)|*.jpg; *.jpeg; *.gif; *.bmp; *.png";
            if (open.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = new Bitmap(open.FileName);
                piclocation = open.FileName;
                textBox7.Text = piclocation;
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            if((textBox3.Text!="")&&(textBox8.Text!="")&&(textBox1.Text!="")){
                MySqlCommand cmd9 = new MySqlCommand("insert into students(studentID,surname,otherNames,gender,address,phone,idNo,year, courseID,academicLevel,sponsorship,passportPhoto,fingerprintID) values('" + textBox8.Text + "','" + textBox1.Text + "','" + textBox2.Text + "','" + comboBox1.SelectedItem + "','" + textBox4.Text + "','" + textBox5.Text + "','" + textBox6.Text + "','" + Convert.ToInt32(comboBox2.SelectedItem) + "','" + comboBox5.SelectedItem + "','" + comboBox3.SelectedItem + "','" + comboBox4.SelectedItem + "',@filepath,'" + Convert.ToInt32(textBox3.Text) + "')", cs);
                cmd9.Parameters.AddWithValue("@filepath", textBox7.Text);
                cs.Open();
                MySqlDataReader Df = cmd9.ExecuteReader();
                while (Df.Read())
                {
                }
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                textBox6.Clear();
                textBox7.Clear();
                textBox8.Clear();
                comboBox1.SelectedItem = null;
                comboBox2.SelectedItem = null;
                comboBox3.SelectedItem = null;
                comboBox4.SelectedItem = null;
                comboBox5.SelectedItem = null;
                pictureBox1.Image = null;
            }
            else{
                DialogResult dr = MessageBox.Show("Please Fill all the necessary details!!");
            }

            cs.Close();
        }
        private void exit_Click(object sender, EventArgs e)
        {
            this.Hide();
            admin_dashboard fm = new admin_dashboard();
            fm.Show();
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void students_enroll_Load(object sender, EventArgs e)
        {
            MySqlCommand cmd = new MySqlCommand("Select courseID,courseName from courses", cs);
            cs.Open();
            MySqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox5.Items.Add(DR[0]);

            }
            cs.Close();
        }
    }
}
