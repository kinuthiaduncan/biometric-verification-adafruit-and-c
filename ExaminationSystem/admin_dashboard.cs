﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExaminationSystem
{
    public partial class admin_dashboard : Form
    {
        public admin_dashboard()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            students_enroll fm = new students_enroll();
            fm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            timetable_creator fm = new timetable_creator();
            fm.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            courses fm = new courses();
            fm.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            course_units fm = new course_units();
            fm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            course_registration fm = new course_registration();
            fm.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            fees fm = new fees();
            fm.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            choose fm = new choose();
            fm.Show();
        }
    }
}
