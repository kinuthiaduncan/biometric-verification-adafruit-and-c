﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO.Ports;

namespace ExaminationSystem
{

    public partial class login : Form
    {
             MySqlConnection cs = new MySqlConnection("Server=localhost;Database=project;UID=projectadmin;Password=iamnumberfour2");
            SerialPort port;
            
        public login()
        {
            InitializeComponent();

        }
        
        private void richTextBox1_Click(object sender, EventArgs e)
        {
            port = new SerialPort("COM5", 9600);
            port.Open();
            if (textBox1.Text == "")
            {
                MessageBox.Show("Please provide a UserName First!!!");
                return;
            }
            try
            {
                
                port.Write("p");
                String myVar = port.ReadLine();
                richTextBox1.Text = myVar;
                //Create SqlConnection
                // MySqlConnection con = new MySqlConnection(cs);
                MySqlCommand cmd = new MySqlCommand("Select * from lecturers where lecturerID=@username and fingerprintID=@myVar", cs);
                cmd.Parameters.AddWithValue("@username", textBox1.Text);
                cmd.Parameters.AddWithValue("@myVar", richTextBox1.Text);
                cs.Open();
                MySqlDataAdapter adapt = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                
                int count = ds.Tables[0].Rows.Count;
                //If count is equal to 1, than show frmMain form
                if (count == 1)
                {
                    string Query = "update progress set supervisor='" + this.textBox1.Text + "' where progressID='1';"; 
                    MySqlCommand MyCommand2 = new MySqlCommand(Query, cs);
                    MySqlDataReader MyReader2;
                   
                    MyReader2 = MyCommand2.ExecuteReader(); 
                    this.Hide();
                    cs.Close();
                   port.Close();
                    choose fm = new choose();
                    fm.Show();
                }
                else
                {
                    MessageBox.Show("Please make sure you have entered the correct details!!");
                    port.Close();
                    cs.Close();
                    login fm = new login();                  
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            ////port.Close();

        }
        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
    }
}
