﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExaminationSystem
{
    public partial class choose : Form
    {
        public choose()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            admin_dashboard fm = new admin_dashboard();
            fm.Show();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            exam_select fm = new exam_select();
            fm.Show();
        }
    }
}
